.PHONY: all clean

CC = gcc
CFLAGS = -Wall -c -std=c11
OBJECTS = io.o
EXECUTABLES = copy count

all: $(EXECUTABLES)

io.o: io.h io.c

copy: main.c $(OBJECTS)
	$(CC) -o $@ $^

count: main.c $(OBJECTS)
	$(CC) -o $@ $^

clean:
	-rm $(EXECUTABLES) $(OBJECTS) *~

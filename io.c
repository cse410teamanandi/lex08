#include <stdio.h>
#include "io.h"

void copyFile(char * inFile, char * outFile){

  FILE *ifp;
  ifp = fopen(inFile,"r");
  if (ifp == NULL) {
    printf("Could not open input file: \"%s\"\n",inFile);
    return;
  }

  FILE *ofp;
  ofp = fopen(outFile,"w");
  if (ofp == NULL) {
    printf("Could not open output file: \"%s\"\n",outFile);
    return;
  }
  char ch;
  while ( (ch = (char) fgetc(ifp)) != EOF ) {
    fputc(ch, ofp);
  }
  
  fclose(ifp);
  fclose(ofp);
}


int countLines(char * inFile) {
  FILE *ifp;
  ifp = fopen(inFile,"r");
  if (ifp == NULL) {
    printf("Could not open input file: \"%s\"\n",inFile);
    return -1;
  }

  char ch;
  int lineCount = 0;
  while ( (ch = (char) fgetc(ifp)) != EOF ) {
    if (ch == '\n') { lineCount++; }
  }
  
  fclose(ifp);
  return lineCount;
}

int countChar(char* inFile) {
  char ch;
  int count = 0;
  FILE *fil
    fil=fopen(inFile,w);
  if(fil=NULL) {
    printf("File DNE");
  }
  while((ch=fgetc(fil))!=EOF) {
    count++;
     }
  return count;
}

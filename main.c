#include <stdio.h>
#include <string.h>
#include "io.h"

int main(int argc, char * argv[]) {

  /* this function initially does the same thing as the one in the master branch however, if it encounter argv[0] i.e. the first input argument as './copy', it performs a copyfile function on the files provided in argv[1] and [2]. This method is declared in io.c. Finally, if the first input arg i.e. argv[0] is './count', it calls the countfile method on the file passed in argv[1] and prints the number of lines in the same  */
  for (int i=0; i<argc; i++) {
    printf("argv[%d] is \"%s\"\n",i,argv[i]);
  }
  if (argc == 3 && strcmp(argv[0],"./copy") == 0) {
    copyFile(argv[1],argv[2]);
  }
  if (argc == 3 && strcmp(argv[0],"./count") && strcmp(argv[1],"lines") == 0) {
    printf("Lines: %d\n",countLines(argv[1]));
  }
  if (argc == 3 && strcmp(argv[0],"./count") && strcmp(argv[1],"chars") == 0) {
    printf("Lines: %d\n",countChar(argv[1]));
  }
  /* prints the correct format for using copy and count as defined in the feature branch */
  printf("Usage:\n\tcopy in out\n\tcount in\n");
  return 0;
}

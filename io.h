#ifndef IO_H
#define IO_H
/* As defined in io.c, copyFile checks whether the files passed in exist and if they do, copies the contents of inFile to outFile. countFile checks if the file inFile exists and return the number of lines in the file */
void copyFile(char * inFile, char * outFile);
int countLines(char * inFile);
int countChar(char * inFile);

#endif
